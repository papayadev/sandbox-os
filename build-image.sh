#!/bin/bash
set -e
./clean.sh
cmake -S . -B cmake-build
cmake --build cmake-build --target disk.img
