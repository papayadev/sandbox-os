#!/bin/bash
set -e
QEMU="qemu-system-x86_64"
FLAGS="-m 4096 -monitor stdio -display gtk"
#FLAGS="-m 4096 -monitor stdio -enable-kvm -vnc :0"
IMG="cmake-build/bin/disk.img"
if [ ! -z "$1" ]; then IMG="$1"; fi
#vncviewer localhost >/dev/null 2>&1 &
${QEMU} ${FLAGS} -drive file=${IMG},format=raw
