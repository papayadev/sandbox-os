#!/bin/bash
set -e
VBOX="VBoxSDL"
VBOXMANAGE="VBoxManage"
VM="Sandbox"
IMG="cmake-build/bin/disk.img"
VDI="cmake-build/bin/disk.vdi"
rm -f "$VDI"
"$VBOXMANAGE" convertdd "$IMG" "$VDI" --format VDI
"$VBOX" --hda "$VDI" --startvm "$VM" --nograbonclick > /dev/null 2>&1 &
