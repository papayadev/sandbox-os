#!/bin/bash
set -e
IMG="cmake-build/bin/disk.img"
KERNEL="cmake-build/bin/kernel"
if [ ! -z "$1" ]; then IMG="$1"; fi
if [ ! -z "$2" ]; then KERNEL="$2"; fi
sudo -v
LOOP=`sudo losetup -f`
sudo losetup -P "$LOOP" "$IMG"
sudo mount "${LOOP}p1" /mnt
sudo mkdir -p /mnt/i386
sudo cp "$KERNEL" /mnt/i386/
sudo sync
sudo umount /mnt
sudo losetup -d "$LOOP"
