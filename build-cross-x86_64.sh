#!/bin/bash
set -e
mkdir -p "$HOME/src/build-binutils"
mkdir -p "$HOME/src/build-gcc"
cd "$HOME/src"
wget -nc https://ftp.gnu.org/gnu/binutils/binutils-2.38.tar.xz
wget -nc https://ftp.gnu.org/gnu/gcc/gcc-11.2.0/gcc-11.2.0.tar.xz
tar -I pixz -xf binutils-*.tar.xz
tar -I pixz -xf gcc-*.tar.xz

export MAKEFLAGS="-j 32"
PREFIX="$HOME/opt/cross-x86_64"
TARGET=x86_64-elf
PATH="$PREFIX/bin:$PATH"

cd "$HOME/src/build-binutils"
../binutils-*/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
make && make install

cd "$HOME/src/gcc-"*"/"
echo "# Add libgcc multilib variant without red-zone requirement" >> gcc/config/i386/t-x86_64-elf
echo "" >> gcc/config/i386/t-x86_64-elf
echo "MULTILIB_OPTIONS += mno-red-zone" >> gcc/config/i386/t-x86_64-elf
echo "MULTILIB_DIRNAMES += no-red-zone" >> gcc/config/i386/t-x86_64-elf
sed -i 's|x86_64-\*-elf\*)|x86_64-\*-elf\*)\n\ttmake_file="\$\{tmake_file\} i386\/t-x86_64-elf" \# include the new multilib configuration|' gcc/config.gcc

cd "$HOME/src/build-gcc"
../gcc-*/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers
make all-gcc && make all-target-libgcc && make install-gcc && make install-target-libgcc
