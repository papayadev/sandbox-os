;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Sandbox OS
;;
;;  Developed by:   Papaya
;;  Email:          me (AT) papayadev.net
;;

	BITS	32

	GLOBAL	IOWait
	GLOBAL	Timer_ISR

	EXTERN	KCODE
	EXTERN	KDATA

Counter:
	dd		0x00000000

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;	ASM procedure: IOWait(uint32)
;;
;;	Stops execution and waits for a given number of milliseconds.
;;
;;	Registers changed: None (Stacks EAX, ECX, EDX)
;;

IOWait:
	push	EBP
	mov		EBP, ESP
	push	EAX
	push	ECX
	push	EDX

	mov		EAX, [EBP+0x08]
	mov		ECX, 0x32
	xor		EDX, EDX
	div		ECX
	mov		[Counter], EAX

.test:
	mov		EAX, [Counter]
	test	EAX, EAX
	jz		.done
	hlt
	jmp		.test

.done:
	pop		EDX
	pop		ECX
	pop		EAX
	pop		EBP
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;	ASM procedure: Timer_ISR()
;;
;;	Interrupt handler for the programmable interrupt timer (IRQ0).
;;
;;	Registers changed: None
;;

Timer_ISR:
	pushad
	push	DS

	mov		AX, KDATA
	mov		DS, AX

	mov		EBX, 0x000B8000
	mov		EDI, 0x9E

	mov		EAX, [Counter]
	test	EAX, EAX
	jz		.space

	dec		EAX
	test	EAX, EAX
	jz		.skip

	mov		ECX, 0x0A

.divide:
	xor		EDX, EDX		
	div		ECX
	add		EDX, 0x30

	mov		[EBX+EDI], DL
	sub		EDI, 0x02

	test	EAX, EAX
	jnz		.divide

.skip:
	dec		dword [Counter]

.space:
	cmp		EDI, 0x8C
	jz		.done

	mov		byte [EBX+EDI], 0x20
	sub		EDI, 0x02

	jmp		.space

.done:
	mov		AL, 0x20
	out		0x20, AL

	pop		DS
	popad
	iret

