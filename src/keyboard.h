/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

#include <stdint.h>
#include "common.h"

uint16_t GetKey(void);

#endif // __KEYBOARD_H__
