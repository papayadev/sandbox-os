/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#ifndef __DEBUG_H__
#define __DEBUG_H__

#include <stdint.h>

void PrintMemoryDump(uint32_t, uint32_t);

#endif // __DEBUG_H__