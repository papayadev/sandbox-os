/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#include <stdint.h>
#include "common.h"

#ifndef __TESTING_H__
#define __TESTING_H__

void EXT2_TEST1(void);

void EXT2_TEST2(void);

void ACPI_TEST1(void);

void FDC_TEST1(void);

#endif // __TESTING_H__