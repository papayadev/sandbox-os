/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#include <stddef.h>
#include <stdint.h>

#include "common.h"
#include "display.h"
#include "string.h"

const uint16_t SizeX = 80;
const uint16_t SizeY = 24;
const uint16_t Top = 1;
const uint16_t Bottom = 25;

uint8_t cursor_x;
uint8_t cursor_y;

uint16_t CursorX = 0;
uint16_t CursorY = 0;

uint8_t CharAttrib = DEFAULT_CHAR_ATTRIB;

volatile uint16_t *const VGABuffer = (uint16_t *) 0x000B8000;

void InitializeStatusBar() {
  uint16_t Index = 0;
  for (Index = 0; Index < 80; Index++) {
    VGABuffer[Index] = (FG_WHITE | BG_RED) << 8;
  }

  return;
}

void PrintStatusBar(char *Message) {
  uint16_t Index = 0;
  for (Index = 0; Message[Index] != 0x00; Index++) {
    VGABuffer[Index] = ((FG_WHITE | BG_RED) << 8) | Message[Index];
  }

  return;
}

void SetAttrib(uint8_t Attrib) {
  CharAttrib = Attrib;
  return;
}

void SetCursor() {
  uint16_t offset;

  outb(0x0F, 0x03D4);
  offset = cursor_x + cursor_y * 80;
  outb(offset & 0xFF, 0x03D5);
  outb(0x0E, 0x03D4);
  outb(offset >> 8, 0x03D5);

  return;
}

void SetCursorXY(uint16_t X, uint16_t Y) {

  uint16_t offset;

  cursor_x = CursorX = X;
  cursor_y = CursorY = Y;

  outb(0x0F, 0x03D4);
  offset = cursor_x + cursor_y * 80;
  outb(offset & 0xFF, 0x03D5);
  outb(0x0E, 0x03D4);
  outb(offset >> 8, 0x03D5);

  return;
}

void ClearDisplay() {
  int32_t position;

  for (position = 0; position < 2000; position++)
    VGABuffer[position] = (CharAttrib << 8) + 0x00;

  SetCursorXY(0, 0);

  return;
}

void ClearConsole() {
  uint16_t Index = 0;

  for (Index = Top * SizeX; Index < Bottom * SizeX; Index++) {
    VGABuffer[Index] = CharAttrib << 8;
  }
  SetCursorXY(0, Top);

  return;
}

void ScrollDisplay() {
  // int32_t position;

  // for (position= 0; position<1920; position++)
  //     VGABuffer[position] = VGABuffer[position+80];

  // for (position= 1920; position<2000; position++)
  //     VGABuffer[position] = (CharAttrib << 8) + 0x00;

  ScrollConsole(Up, 1);

  return;
}

void ScrollConsole(DIRECTION Direction, uint16_t Count) {
  uint16_t Index = 0;

  while (Count > 0) {
    if (Direction == Up) {
      for (Index = Top * SizeX; Index < Bottom * SizeX; Index++) {
        if (Index < (Bottom - 1) * SizeX) {
          VGABuffer[Index] = VGABuffer[Index + SizeX];
        } else {
          VGABuffer[Index] = CharAttrib << 8;
        }
      }
    } else if (Direction == Down) {
      for (Index = Bottom * SizeX; Index > Top * SizeX; Index--) {
        if (Index > (Top + 1) * SizeX) {
          VGABuffer[Index] = VGABuffer[Index - SizeX];
        } else {
          VGABuffer[Index] = CharAttrib << 8;
        }
      }
    }
    Count--;
  }

  return;
}

void PutChar(uint8_t character) {
  switch (character) {
    case 0x08:
      if (cursor_x) {
        cursor_x--;
        VGABuffer[cursor_x + cursor_y * 80] = (CharAttrib << 8) + 0x00;
      } else if (cursor_y) {
        cursor_y--;
        cursor_x = 79;
        VGABuffer[cursor_x + cursor_y * 80] = (CharAttrib << 8) + 0x00;
      }

      break;

    case 0x09:
      cursor_x = (cursor_x + 8) / 8;
      cursor_x = cursor_x * 8;

      break;

    case 0x0A:
      cursor_y++;
      cursor_x = 0;

      break;

    case 0x0D:
      cursor_y++;
      cursor_x = 0;

      break;
  }

  if (character >= 0x20) {
    VGABuffer[cursor_x + cursor_y * 80] = (CharAttrib << 8) + character;
    cursor_x++;
  }

  if (cursor_x >= 80) {
    cursor_x = 0;
    cursor_y++;
  }

  if (cursor_y >= 25) {
    ScrollDisplay();
    cursor_y--;
  }

  SetCursor();

  return;
}

char Hex[16] = "0123456789ABCDEF";

void PutBYTE(uint8_t Value) {
  PutChar(Hex[(Value / 16) % 16]);
  PutChar(Hex[Value % 16]);
}

void PutUINT16(uint16_t Value) {
  PutBYTE(Value >> 8);
  PutBYTE(Value & 0xFF);
}

void PutUINT32(uint32_t Value) {
  PutUINT16(Value >> 16);
  PutUINT16(Value & 0xFFFF);
}

char Dec[10] = "0123456789";
char DecText[11] = "4294967295";

void PrintUINT32(uint32_t Value) {
  uint8_t Length = 0;
  char Text[11] = "0";

  if (Value == 0) {
    PutChar(Text[0]);
    return;
  }

  for (uint32_t Temp = Value; Temp > 0; Temp /= 10) {
    Length++;
  }
  Text[Length] = 0x00;
  for (; Length > 0; Length--) {
    Text[Length - 1] = Dec[Value % 10];
    Value /= 10;
  }

  PrintString(Text);
}
