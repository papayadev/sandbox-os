/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#include <stdint.h>

#include "common.h"
#include "isa_dma.h"

void IOWait(uint32_t);

void DMA_FDC_Initialize() {
  outb(0x06, 0x0A); // Mask DMA 0 & 2
  outb(0xFF, 0x0C); // Reset Flip-Flop
  outb(0x00, 0x04); // Start Address LOW( 0x0000 )
  outb(0x00, 0x04); // Start Address HIGH( 0x0000 )
  outb(0xFF, 0x0C); // Reset Flip-Flop
  outb(0xFF, 0x05); // Count LOW( 0x23FF )
  outb(0x23, 0x05); // Count HIGH( 0x23FF )
  outb(0x20, 0x81); // External Page Register ( 0x20 )
  outb(0x02, 0x0A);

  // return;
}

void DMA_FDC_Configure_Write() {
  outb(0x06, 0x0A); // Mask DMA 0 & 2
  outb(0x4A, 0x0B); // Set DMA Mode
  outb(0x02, 0x0A); // Unmask DMA 2

  return;
}

void DMA_FDC_Configure_Read() {
  outb(0x06, 0x0A); // Mask DMA 0 & 2
  outb(0x46, 0x0B); // Set DMA Mode
  outb(0x02, 0x0A); // Unmask DMA 2

  return;
}
