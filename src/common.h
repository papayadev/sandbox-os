/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdint.h>

#define RETURN_FALSE(TEST) \
  if (TEST)                \
  {                        \
    return false;          \
  }
#define RETURN_TRUE(TEST) \
  if (TEST)               \
  {                       \
    return true;          \
  }

uint8_t inb(uint16_t);

void outb(uint8_t, uint16_t);

#endif // __COMMON_H__
