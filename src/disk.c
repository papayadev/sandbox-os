/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "common.h"
#include "debug.h"
#include "disk.h"
#include "display.h"
#include "isa_dma.h"
#include "string.h"

void IOWait(uint32_t);

bool FDC_Wait_IRQ = false;

////////////////////////////////////////////////////////////////////////////////
//
//  void FDC_Read/WriteTrackDMA( FDC_DRIVE Drive, uint8_t Cylinder, uint8_t Head )
//
//  Always reads/writes track into/from memory at 2MB ( 0x00200000 )
//

void FDC_ReadTrackDMA(FDC_DRIVE Drive, uint8_t Cylinder, uint8_t Head) {
  DMA_FDC_Configure_Read();
  DMA_FDC_Initialize();
  FDC_SelectDrive(Drive);
  FDC_Specify();
  FDC_StartMotor(Drive);
  FDC_Seek(Drive, Cylinder);
  FDC_ReadTrack(Drive, Cylinder, Head);
  FDC_StopMotor(Drive);

  return;
}

void FDC_WriteTrackDMA(FDC_DRIVE Drive, uint8_t Cylinder, uint8_t Head) {
  DMA_FDC_Configure_Write();
  DMA_FDC_Initialize();
  FDC_SelectDrive(Drive);
  FDC_Specify();
  FDC_StartMotor(Drive);
  FDC_Seek(Drive, Cylinder);
  FDC_WriteTrack(Drive, Cylinder, Head);
  FDC_StopMotor(Drive);

  return;
}

////////////////////////////////////////////////////////////////////////////////
//
//  bool FDC_Initialize( void )
//
//  1. Check Version
//  2. Configure
//  3. Lock
//  4. Reset
//  5. Recalibrate
//

bool FDC_Initialize(void) {
  PrintString("Version Check...      ");
  if (FDC_GetVersion() != 0x90) // Verify 82077AA FDC
  {
    PrintString("[FAIL]");
    return false; // Otherwise, false
  }
  PrintString("[OK]\n");
  PrintString("Configure...          ");
  FDC_Configure();
  PrintString("[OK]\n");
  PrintString("Lock...               ");
  FDC_Lock();
  PrintString("[OK]\n");
  PrintString("Reset Controller...   ");
  FDC_ResetController();
  PrintString("[OK]\n");
  PrintString("Recalibrate MASTER... ");
  FDC_Recalibrate(FDC_MASTER);
  PrintString("[OK]\n");
  PrintString("Recalibrate SLAVE...  ");
  FDC_Recalibrate(FDC_SLAVE);
  PrintString("[OK]\n");

  return true;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void FDC_SendCommandByte(FDC_COMMAND Command) {
  FDC_WaitReadyForOutput();
  // PutChar( '!' );
  // PutBYTE( Command );
  outb(Command, FDC_DATA_FIFO);
  return;
}

void FDC_SendParameterByte(uint8_t Parameter) {
  FDC_WaitReadyForOutput();
  // PutChar( '<' );
  // PutBYTE( Parameter );
  outb(Parameter, FDC_DATA_FIFO);
  return;
}

uint8_t FDC_GetResultByte(void) {
  FDC_WaitReadyForInput();
  uint8_t Result = inb(FDC_DATA_FIFO);
  // PutChar( '>' );
  // PutBYTE( Result );
  return Result;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

bool FDC_WaitReadyForInput(void) {
  uint8_t Status = 0;
  do {
    do {
      Status = inb(FDC_MAIN_STATUS);
    } while (!(Status & FDC_MSR_RQM));
  } while (!(Status & FDC_MSR_DIO));
  return true;
}

bool FDC_WaitReadyForOutput(void) {
  uint8_t Status = 0;
  do {
    do {
      Status = inb(FDC_MAIN_STATUS);
    } while (!(Status & FDC_MSR_RQM));
  } while ((Status & FDC_MSR_DIO));

  return true;
}

////////////////////////////////////////////////////////////////////////////////
//
//  uint8_t FDC_GetVersion( void )
//
//  Sends VERSION command to FDC and returns VERSION
//  Return: 0x00 indicates failure
//

uint8_t FDC_GetVersion(void) {
  FDC_SendCommandByte(FDC_VERSION);
  return FDC_GetResultByte();
}

////////////////////////////////////////////////////////////////////////////////
//
//  bool FDC_Configure( void )
//
//  Sends CONFIGRE command to FDC and returns SUCCESS
//  1st parameter: Always 0x00
//  2nd parameter: Lower 4 bits set byte threshold
//  2nd parameter: Upper 4 bits set options
//  3rd parameter: Write precompensation, 0x00 uses manufacturer default
//

bool FDC_Configure(void) {
  FDC_SendCommandByte(FDC_CONFIGURE);
  FDC_SendParameterByte(0x00);
  FDC_SendParameterByte(
      0x07 | FDC_DISABLE_POLLING | FDC_IMPLIED_SEEK | FDC_DISABLE_FIFO);
  // FDC_SendParameterByte( 0x07 | FDC_IMPLIED_SEEK );
  FDC_SendParameterByte(0x00);

  return true;
}

////////////////////////////////////////////////////////////////////////////////
//
//  bool FDC_LOCK( void )
//
//  Sends LOCK command to FDC and returns SUCCESS
//  RESULT: 0x10 indicates SUCCESS
//

bool FDC_Lock(void) {
  uint8_t Result;
  FDC_SendCommandByte(FDC_LOCK);
  Result = FDC_GetResultByte();
  if (Result != 0x10) {
    return false;
  }

  return true;
}

////////////////////////////////////////////////////////////////////////////////
//
//  bool FDC_ResetController( void )
//
//  Setting bit 4 of the DSR initiates a controller reset
//  It may be necessary to select a drive afterward
//

bool FDC_ResetController(void) {
  uint8_t DSRValue = 0;

  DSRValue = inb(FDC_DATARATE_SELECT);
  DSRValue |= 0x80;
  outb(DSRValue, FDC_DATARATE_SELECT);

  FDC_SenseInterrupt();
  FDC_SenseInterrupt();
  FDC_SenseInterrupt();
  FDC_SenseInterrupt();

  FDC_SelectDrive(FDC_MASTER);

  return true;
}

////////////////////////////////////////////////////////////////////////////////
//
//  bool FDC_SelectDrive( FDC_DRIVE Drive )
//
//  CCR: 0-1 Data Rate, 2-7 Always 0x00 (Unused)
//  Must set values for CCR and DOR to select drive

bool FDC_SelectDrive(FDC_DRIVE Drive) {
  uint8_t DORValue = 0;

  outb(0x00, FDC_CONFIGURATION_CONTROL_REGISTER);

  // Should a SPECIFY command be sent here?

  DORValue = inb(FDC_DIGITAL_OUTPUT_REGISTER);
  DORValue &= 0xFC;
  DORValue |= Drive;
  outb(DORValue, FDC_DIGITAL_OUTPUT_REGISTER);

  return true;
}

////////////////////////////////////////////////////////////////////////////////
//
//  bool FDC_Recalibrate( FDC_DRIVE Drive )
//

bool FDC_Recalibrate(FDC_DRIVE Drive) {
  FDC_Wait_IRQ = true;
  FDC_SendCommandByte(FDC_RECALIBRATE);
  FDC_SendParameterByte(Drive);
  while (FDC_Wait_IRQ);
  FDC_SenseInterrupt();

  return true;
}

void FDC_SenseInterrupt() {
  FDC_SendCommandByte(FDC_SENSE_INTERRUPT);
  FDC_GetResultByte();
  FDC_GetResultByte();

  return;
}

////////////////////////////////////////////////////////////////////////////////
//
//  void FDC_StartMotor( FDC_Drive Drive )
//
//  Start the motor of the specified floppy disk drive.
//  Setting the appropriate bit in the "Digital Output Register" will do this.
//

void FDC_StartMotor(FDC_DRIVE Drive) {
  uint8_t DORValue = inb(FDC_DIGITAL_OUTPUT_REGISTER);
  // Read the DOR
  if (Drive == FDC_MASTER) {
    DORValue |= FDC_DOR_MOTA; // Set bit 4 to turn drive 0 on
  } else if (Drive == FDC_SLAVE) {
    DORValue |= FDC_DOR_MOTB; // Set bit 5 to turn drive 1 on
  } else {         // Invalid drive specifed
    return; // Do nothing and return immediately
  }
  outb(DORValue, FDC_DIGITAL_OUTPUT_REGISTER);
  // Write modified value to DOR
  IOWait(50); // Wait 300 ms for drive to spin up

  return;
}

////////////////////////////////////////////////////////////////////////////////
//
//  void FDC_StopMotor( FDC_Drive Drive )
//
//  Stop the motor of the specified floppy disk drive.
//  Clearing the appropriate bit in the "Digital Output Register" will do this.
//

void FDC_StopMotor(FDC_DRIVE Drive) {
  uint8_t DORValue = inb(FDC_DIGITAL_OUTPUT_REGISTER);
  // Read the DOR
  if (Drive == FDC_MASTER) {
    DORValue &= ~FDC_DOR_MOTA; // Clear bit 4 to turn drive 0 off
  } else if (Drive == FDC_SLAVE) {
    DORValue &= ~FDC_DOR_MOTB; // Clear bit 5 to turn drive 1 off
  } else {         // Invalid drive specified
    return; // Do nothing and return immediately
  }
  outb(DORValue, FDC_DIGITAL_OUTPUT_REGISTER);
  // Write modified value to DOR
  FDC_Seek(Drive, 0); // Seek the head back to cylinder 0
  IOWait(50);         // wait 1000 ms for drive to stop

  return;
}

////////////////////////////////////////////////////////////////////////////////
//
//  void FDC_CheckMedia( FDC_Drive Drive )
//
//  NOT IMPLIMENTED!  WILL RETURN false!
//

bool FDC_CheckMedia(/* FDC_DRIVE Drive */) {
  return false;
}

////////////////////////////////////////////////////////////////////////////////
//
//  void FDC_Specify( void )
//
//  FDC Command: Specify
//  Configure safe timings for the heads of a 1.44MB floppy disk drive.
//  This applies to all connected drives.
//

void FDC_Specify(void) {
  uint8_t FDC_SRT = 0x08 << 4; // Step Rate Time (8 ms)
  uint8_t FDC_HLT = 0x05 << 1; // Head Load Time (10 ms)
  uint8_t FDC_HUT = 0x00;      // Head Unload Time (Default: Max)

  FDC_SendCommandByte(FDC_SPECIFY); // Specify Command
  FDC_SendParameterByte(FDC_SRT | FDC_HUT);
  // Byte 1: SRT and HUT
  FDC_SendParameterByte(FDC_HLT); // Byte 2: HLT

  return;
}

////////////////////////////////////////////////////////////////////////////////
//
//  void FDC_Seek( FDC_DRIVE Drive, uint8_t Cylinder )
//
//  FDC Command: Seek
//  Seek to a cylinder on a floppy disk drive.
//  If "Implied Seek" is used, calling this is unnecessary.
//

void FDC_Seek(FDC_DRIVE Drive, uint8_t Cylinder) {
  FDC_SendCommandByte(FDC_SEEK);            // Seek Command
  FDC_SendParameterByte(0x00 << 2 | Drive); // Byte 1: Head 0, Drive #
  FDC_Wait_IRQ = true;
  FDC_SendParameterByte(Cylinder); // Byte 2: Cylinder #
  while (FDC_Wait_IRQ);                   // Wait for an IRQ6
  FDC_SenseInterrupt(); // Acknowledge IRQ6

  return;
}

void FDC_ReadTrack(FDC_DRIVE Drive, uint8_t Cylinder, uint8_t Head) {
  FDC_SendCommandByte(0xE6);
  FDC_SendParameterByte(Head << 2 | Drive); // Head 0, Master
  FDC_SendParameterByte(Cylinder);          // Cylinder 0
  FDC_SendParameterByte(Head);              // Head 0
  FDC_SendParameterByte(0x01);              // Sector 1
  FDC_SendParameterByte(0x02);              // 512 Byte Sectors
  FDC_SendParameterByte(0x12);              // 18 Sectors per Track
  FDC_SendParameterByte(0x1B);              // GAP1 Size (Default)
  FDC_Wait_IRQ = true;
  FDC_SendParameterByte(0xFF); // 512 Byte Sectors
  while (FDC_Wait_IRQ);
  FDC_GetResultByte();
  FDC_GetResultByte();
  FDC_GetResultByte();
  FDC_GetResultByte();
  FDC_GetResultByte();
  FDC_GetResultByte();
  FDC_GetResultByte();

  return;
}

void FDC_WriteTrack(FDC_DRIVE Drive, uint8_t Cylinder, uint8_t Head) {
  FDC_SendCommandByte(0xE5);
  FDC_SendParameterByte(Head << 2 | Drive); // Head 0, Master
  FDC_SendParameterByte(Cylinder);          // Cylinder 0
  FDC_SendParameterByte(Head);              // Head 0
  FDC_SendParameterByte(0x01);              // Sector 1
  FDC_SendParameterByte(0x02);              // 512 Byte Sectors
  FDC_SendParameterByte(0x12);              // 18 Sectors per Track
  FDC_SendParameterByte(0x1B);              // GAP1 Size (Default)
  FDC_Wait_IRQ = true;
  FDC_SendParameterByte(0xFF); // 512 Byte Sectors
  while (FDC_Wait_IRQ);
  FDC_GetResultByte();
  FDC_GetResultByte();
  FDC_GetResultByte();
  FDC_GetResultByte();
  FDC_GetResultByte();
  FDC_GetResultByte();
  FDC_GetResultByte();

  return;
}
