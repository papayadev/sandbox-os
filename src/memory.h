/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#ifndef __MEMORY_H__
#define __MEMORY_H__

#include <stdint.h>

void Init_Paging(void);

#endif // __MEMORY_H__
