/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#ifndef __EXT2_H__
#define __EXT2_H__

#include <stdbool.h>
#include <stdint.h>

typedef struct {
    uint32_t TotalInodes;
    uint32_t TotalBlocks;
    uint32_t ReservedBlocks;
    uint32_t UnallocatedBlocks;
    uint32_t UnallocatedInodes;
    uint32_t ThisBlock;
    uint32_t BlockSize;    // 1024 << BlockSize
    uint32_t FragmentSize; // 1024 << FragmentSize
    uint32_t BlocksPerGroup;
    uint32_t FragmentsPerGroup;
    uint32_t InodesPerGroup;
    uint32_t LastMountTime;
    uint32_t LastWriteTime;
    uint16_t TimesMountedSinceCheck;
    uint16_t TimesMountedUntilCheck;
    uint16_t Signature;
    uint16_t FileSystemState;
    uint16_t ErrorInstructions;
    uint16_t VersionMinor;
    uint32_t LastCheckTime;
    uint32_t TimeBetweenChecks;
    uint32_t OSID;
    uint32_t VersionMajor;
    uint16_t ReservedUserID;
    uint16_t ReservedGroupID;
} EXT2_SUPERBLOCK;

typedef struct {
    uint32_t FirstNonReservedInode;
    uint16_t InodeSize;
    uint16_t ThisBlockGroup;
    uint32_t OptionalFeatures;
    uint32_t RequiredFeatures;
    uint32_t WriteRequiredFeatures;
    uint8_t FileSystemID[16];
    uint8_t VolumeName[16];
    uint8_t PathLastMountedTo[64];
    uint32_t CompressionTypeUsed;
    uint8_t BlocksToPreallocateForFiles;
    uint8_t BlocksToPreallocateForDirectories;
    uint8_t _Unused1[2];
    uint32_t JournalID;
    uint32_t JournalInode;
    uint32_t JournalDevice;
    uint32_t OrphanInodeListHead;
    uint8_t _Unused2[788];

} EXT2_SUPERBLOCK_EXTENDED;

typedef struct {
    uint32_t BlockBitmapBlock;
    uint32_t InodeBitmapBlock;
    uint32_t InodeTableStartBlock;
    uint16_t UnallocatedBlocks;
    uint16_t UnallocatedInodes;
    uint16_t DirectoryCount;
    uint8_t _Unused[14];
} EXT2_BLOCK_GROUP_DISCRIPTOR;

typedef struct {
    uint16_t TypePermissions;
    uint16_t UserID;
    uint32_t SizeLow32;
    uint32_t AccessTime; // POSIX
    uint32_t CreateTime; // POSIX
    uint32_t ModifyTime; // POSIX
    uint32_t DeleteTime; // POSIX
    uint16_t GroupID;
    uint16_t CountHardLinks;
    uint32_t CountSectors;
    uint32_t Flags;
    uint32_t OSValue1;
    uint32_t Block0;
    uint32_t Block1;
    uint32_t Block2;
    uint32_t Block3;
    uint32_t Block4;
    uint32_t Block5;
    uint32_t Block6;
    uint32_t Block7;
    uint32_t Block8;
    uint32_t Block9;
    uint32_t Block10;
    uint32_t Block11;
    uint32_t SingleIndirectBlock;
    uint32_t DoubleIndirectBlock;
    uint32_t TripleIndirectBlock;
    uint32_t Generation;
    uint32_t ExtendedAttributeBlock;
    uint32_t SizeHigh32;
    uint32_t FragmentBlock;
    uint8_t OSValue2[12];
} EXT2_INODE;

typedef struct {
    uint32_t Inode;
    uint16_t EntrySize;
    uint8_t NameLength;
    uint8_t Type;
    uint8_t Name[256]; // Not NULL terminated!
} // Length = .NameLength
EXT2_DIRECTORY_ENTRY;

typedef struct {
    uint32_t Cylinder;
    uint32_t Head;
    uint32_t Sector;
    uint32_t HPC;
    uint32_t SPT;
} DISK_CHS;

extern EXT2_SUPERBLOCK Superblock;
extern EXT2_SUPERBLOCK_EXTENDED SuperblockExt;

bool EXT2_ParseSuperblock(void);

bool LBAtoCHS(uint32_t LBA, DISK_CHS *ptrCHS);

bool GetInode(uint32_t InodeID, EXT2_INODE *Inode);

uint32_t ParseDirectory(EXT2_INODE *Inode, char *FileName);

bool ParseInodeData(EXT2_INODE *Inode);

bool DumpFile(char *FileName);

#endif // __EXT2_H__