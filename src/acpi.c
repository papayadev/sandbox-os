/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#include <stddef.h>
#include <stdint.h>

#include "acpi.h"
#include "common.h"
#include "debug.h"
#include "display.h"
#include "string.h"

uint32_t GetRsdpAddress(void) {
  uint32_t Base = 0;
  uint32_t Index = 0;
  char Pattern[] = "RSD PTR ";

  for (Base = 0; Base < 0x00400000; Base += 0x10) {
    for (Index = 0; Index < 8; Index++) {
      if (*(char *) (Base + Index) != Pattern[Index]) {
        break;
      }
    }
    if (Index == 8) {
      uint32_t Checksum = 0;
      for (Index = 0; Index < 20; Index++)
        Checksum += *(uint8_t *) (Base + Index);
      if ((Checksum & 0xFF) != 0x00)
        continue;
      if (*(uint8_t *) (Base + 0x0F) == 0x00)
        return Base;
      Checksum = 0;
      for (; Index < 36; Index++)
        Checksum += *(uint8_t *) (Base + Index);
      if ((Checksum & 0xFF) == 0x00)
        return Base;
    }
  }

  return 0;
}

uint32_t GetRsdtAddress(void) {
  uint32_t RSDP = GetRsdpAddress();
  if (!RSDP)
    return 0;
  return *(uint32_t *) (RSDP + 0x10);
}

uint32_t GetFacpAddress(void) {
  uint32_t RSDT = GetRsdtAddress();
  if (!RSDT)
    return 0;
  uint32_t Entries = (*(uint32_t *) (RSDT + 0x04) - 0x24) / 0x04;
  RSDT += 0x24;
  char Pattern[] = "FACP";
  for (uint32_t Index = 0; Index < Entries; Index++) {
    uint32_t PIndex = 0;
    uint32_t SDT = *(uint32_t *) (RSDT + (Index * 0x04));
    for (PIndex = 0; PIndex < 4; PIndex++) {
      if (*(char *) (SDT + PIndex) != Pattern[PIndex]) {
        break;
      }
    }
    if (PIndex == 4) {
      return *(uint32_t *) (RSDT + (Index * 0x04));
    }
  }
  return 0;
}

uint32_t GetDsdtAddress(void) {
  uint32_t FACP = GetFacpAddress();
  if (!FACP)
    return 0;
  return *(uint32_t *) (FACP + 0x28);
}

uint32_t GetS5Address(void) {
  uint32_t DSDT = GetDsdtAddress();
  if (!DSDT)
    return 0x10;
  uint32_t DSDT_Data = DSDT + 0x24;
  uint32_t Index = 0, PIndex = 0;
  char Pattern[] = "_S5_";

  for (Index = 0; Index < (*(uint32_t *) (DSDT + 0x04) - 0x28); Index++) {
    for (PIndex = 0; PIndex < 4; PIndex++) {
      if (*(char *) (DSDT_Data + Index + PIndex) != Pattern[PIndex])
        break;
    }
    if (PIndex == 4) {
      return (DSDT_Data + Index);
    }
  }
  return 0x20;
}
