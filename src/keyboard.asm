;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Sandbox OS
;;
;;  Developed by:   Papaya
;;  Email:          me (AT) papayadev.net
;;

BITS 32

EXTERN	KCODE
EXTERN	KDATA

GLOBAL	GetKey
GLOBAL	Keyboard_ISR

KeyBuffer:
	dd	0x00000000
	dd	0x00000000
	dd	0x00000000
	dd	0x00000000
	dd	0x00000000
	dd	0x00000000
	dd	0x00000000
	dd	0x00000000
KeyFirst:
	dd	0x00000000
KeyLast:
	dd	0x00000000
KeyShift:
	dd	0x00000000

ASCII:
	db	0x00, 0x1B, "1" , "2" , "3" , "4" , "5" , "6"	;; 0x00-0x07
	db	"7" , "8" , "9" , "0" , "-" , "=" , 0x08, 0x09	;; 0x08-0x0F
	db	"q" , "w" , "e" , "r" , "t" , "y" , "u" , "i"	;; 0x10-0x17
	db	"o" , "p" , "[" , "]" , 0x0D, 0x00, "a" , "s"	;; 0x18-0x1F
	db	"d" , "f" , "g" , "h" , "j" , "k" , "l" , ";"	;; 0x20-0x27
	db	"'" , "`" , 0x00, "\" , "z" , "x" , "c" , "v"	;; 0x28-0x2F
	db	"b" , "n" , "m" , "," , "." , "/" , 0x00, "*"	;; 0x30-0x37
	db	0x00, " " , 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x38-0x3F
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x40-0x47
	db	0x00, 0x00, "-" , 0x00, 0x00, 0x00, "+" , 0x00	;; 0x48-0x4F
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x50-0x57
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x58-0x5F
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x60-0x67
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x68-0x6F
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x70-0x77
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x78-0x7F

Shift_ASCII:
	db	0x00, 0x1B, "!" , "@" , "#" , "$" , "%" , "^"	;; 0x00-0x07
	db	"&" , "*" , "(" , ")" , "_" , "+" , 0x08, 0x09	;; 0x08-0x0F
	db	"Q" , "W" , "E" , "R" , "T" , "Y" , "U" , "I"	;; 0x10-0x17
	db	"O" , "P" , "{" , "}" , 0x0D, 0x00, "A" , "S"	;; 0x18-0x1F
	db	"D" , "F" , "G" , "H" , "J" , "K" , "L" , ":"	;; 0x20-0x27
	db	'"' , "~" , 0x00, "|" , "Z" , "X" , "C" , "V"	;; 0x28-0x2F
	db	"B" , "N" , "M" , "<" , ">" , "?" , 0x00, "*"	;; 0x30-0x37
	db	0x00, " " , 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x38-0x3F
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x40-0x47
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x48-0x4F
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x50-0x57
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x58-0x5F
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x60-0x67
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x68-0x6F
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x70-0x77
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	;; 0x78-0x7F

NumLock_ASCII:
	db	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, "7"	;; 0x40-0x47
	db	"8" , "9" , "-" , "4" , "5" , "6" , "+" , "1"	;; 0x48-0x4F
	db	"2" , "3" , "0" , "." , 0x00, 0x00, 0x00, 0x00	;; 0x50-0x57

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;	C procedure: GetKey()
;;
;;	Get a key from the keyboard buffer.
;;
;;	Input: N/A
;;	Return: uint16
;;	Registers changed: N/A

GetKey:
	push EBP
  push EBX
	mov	EBP, ESP

	mov	EBX, [KeyFirst]

.next:
	cmp	EBX, [KeyLast]
	jne	.done
	hlt
	jmp	.next
.done:

	inc	EBX
	inc	EBX
	and	EBX, 0x1F
	jnz	.continue
	inc	EBX
	inc	EBX

.continue:
	xor	EAX, EAX
	mov	AX, [KeyBuffer+EBX-0x02]
	mov	[KeyFirst], EBX
  pop EBX
	pop	EBP
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;	ASM procedure: Keyboard_ISR()
;;
;;	Interrupt handler for the keyboard (IRQ1).
;;
;;	Input: N/A
;;	Output: N/A
;;	Registers changed: None

Keyboard_ISR:
	pushad

	push	DS
	mov	EAX, KDATA
	mov	DS, AX

	xor	EAX, EAX
	in	AL, 0x60
	shl	EAX, 0x08

	cmp	AH, 0x3A
	je	near .MAKE_CAPLCK
	cmp	AH, 0x45
	je	near .MAKE_NUMLCK
	cmp	AH, 0x46
	je	near .MAKE_SCRLCK
	cmp	AH, 0x2A
	je	near .MAKE_LSHIFT
	cmp	AH, 0x36
	je	near .MAKE_RSHIFT
	cmp	AH, 0x1D
	je	near .MAKE_LCTRL
	cmp	AH, 0x38
	je	near .MAKE_LALT

	cmp	AH, 0xBA
	je	near .BREAK_CAPLCK
	cmp	AH, 0xC5
	je	near .BREAK_NUMLCK
	cmp	AH, 0xC6
	je	near .BREAK_SCRLCK
	cmp	AH, 0xAA
	je	near .BREAK_LSHIFT
	cmp	AH, 0xB6
	je	near .BREAK_RSHIFT
	cmp	AH, 0x9D
	je	near .BREAK_LCTRL
	cmp	AH, 0xB8
	je	near .BREAK_LALT

	cmp	AH, 0xE0
	je	near .EXT_CODE

	jmp	.TRANSLATE
.MAKE_CAPLCK:
	xor	dword [KeyShift], 0x04

	mov	EAX, 0xED
	out	0x60, AL
	mov	EAX, [KeyShift]
	out	0x60, AL

	jmp	.EOI
.MAKE_NUMLCK:
	xor	dword [KeyShift], 0x02

	mov	EAX, 0xED
	out	0x60, AL
	mov	EAX, [KeyShift]
	out	0x60, AL

	jmp	.EOI
.MAKE_SCRLCK:
	xor	dword [KeyShift], 0x01

	mov	EAX, 0xED
	out	0x60, AL
	mov	EAX, [KeyShift]
	out	0x60, AL

	jmp	.EOI
.MAKE_LSHIFT:
	or	dword [KeyShift], 0x2000
	jmp	.EOI
.MAKE_RSHIFT:
	or	dword [KeyShift], 0x1000
	jmp	.EOI
.MAKE_LCTRL:
	or	dword [KeyShift], 0x0800
	jmp	.EOI
.MAKE_RCTRL:
	or	dword [KeyShift], 0x0400
	jmp	.EOI
.MAKE_LALT:
	or	dword [KeyShift], 0x0200
	jmp	.EOI
.MAKE_RALT:
	or	dword [KeyShift], 0x0100
	jmp	.EOI
.BREAK_CAPLCK:
.BREAK_NUMLCK:
.BREAK_SCRLCK:
	jmp	.EOI
.BREAK_LSHIFT:
	and	dword [KeyShift], 0xDFFF
	jmp	.EOI
.BREAK_RSHIFT:
	and	dword [KeyShift], 0xEFFF
	jmp	.EOI
.BREAK_LCTRL:
	and	dword [KeyShift], 0xF7FF
	jmp	.EOI
.BREAK_RCTRL:
	and	dword [KeyShift], 0xFBFF
	jmp	.EOI
.BREAK_LALT:
	and	dword [KeyShift], 0xFDFF
	jmp	.EOI
.BREAK_RALT:
	and	dword [KeyShift], 0xFEFF
.EXT_CODE:
	in	AL, 0x60
	shl	EAX, 0x08

	cmp	AH, 0x1D
	je	.MAKE_RCTRL
	cmp	AH, 0x38
	je	.MAKE_RALT

	cmp	AH, 0x9D
	je	.BREAK_RCTRL
	cmp	AH, 0xB8
	je	.BREAK_RALT

	jmp	.TRANSLATE
.TRANSLATE:
	test	AH, 0x80		;; test for BREAK code
	jnz	near .EOI		;; and jump to EOI

	xor	EBX, EBX		;; calculate ASCII table offset
	mov	BL, AH

	test	dword [KeyShift], 0x04	;; test CAPSLOCK key
	jnz	.CAPLCK				;; and jump to CAPLCK

	test	dword [KeyShift], 0x02
	jnz	.NUMLCK
.CAPLCK_IGNORE:
.NUMLCK_IGNORE:
	test	dword [KeyShift], 0x3000	;; test SHIFT key
	jnz	.SHIFT				;; and jump to SHIFT
.DEFAULT:
	mov	AL, [ASCII+EBX]		;; convert to ASCII
	jmp	.continue1		;; and continue
.CAPLCK:
	cmp	AH, 0x10		;; IGNORE if below "Q"
	jb	.CAPLCK_IGNORE
	cmp	AH, 0x19		;; USE if "P" or below
	jbe	.CAPLCK_USE
	cmp	AH, 0x1E		;; IGNORE if below "A"
	jb	.CAPLCK_IGNORE
	cmp	AH, 0x26		;; USE if "L" or below
	jbe	.CAPLCK_USE
	cmp	AH, 0x2C		;; IGNORE if below "Z"
	jb	.CAPLCK_IGNORE
	cmp	AH, 0x32		;; USE if "M" or below
	jbe	.CAPLCK_USE
	jmp	.CAPLCK_IGNORE		;; else IGNORE
.CAPLCK_USE:
	test	dword [KeyShift], 0x3000	;; test SHIFT key
	jnz	.DEFAULT			;; and jump to DEFAULT

	jmp	.SHIFT			;; else jump to SHIFT
.NUMLCK:
	cmp	AH, 0x47
	jb	.NUMLCK_IGNORE
	cmp	AH, 0x49
	jbe	.NUMLCK_USE
	cmp	AH, 0x4B
	jb	.NUMLCK_IGNORE
	cmp	AH, 0x4D
	jbe	.NUMLCK_USE
	cmp	AH, 0x4F
	jb	.NUMLCK_IGNORE
	cmp	AH, 0x53
	jbe	.NUMLCK_USE
	jmp	.NUMLCK_IGNORE
.NUMLCK_USE:
	test	dword [KeyShift], 0x3000	;; test SHIFT key
	jnz	.DEFAULT			;; and jump to DEFAULT

	mov	AL, [NumLock_ASCII+EBX-0x40]
	jmp	.continue1
.SHIFT:
	mov	AL, [Shift_ASCII+EBX]	;; convert to Shift_ASCII
	jmp	.continue1		;; and continue
.continue1:
	mov	EBX, [KeyLast]		;; advance LASTKEY pointer
	inc	EBX
	inc	EBX
	and	EBX, 0x1F
	jnz	.continue2
	inc	EBX
	inc	EBX
.continue2:
	cmp	EBX, [KeyFirst]		;; test for full KEYBUFFER
	je	.EOI			;; and jump to EOI

;;	xor	BH, BH			;; place key in KEYBUFFER
	mov	[KeyBuffer+EBX-0x02], AX
	mov	[KeyLast], EBX

	jmp	.EOI
.EOI:
	pop	DS

	mov	AL, 0x20		; EOI
	out	0x20, AL

	popad				; pop all registers
	iret				; and return
