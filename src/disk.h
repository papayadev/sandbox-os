/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#ifndef __DISK_H__
#define __DISK_H__

#include <stdbool.h>
#include <stdint.h>

typedef enum {
    FDC_MASTER = 0x00,
    FDC_SLAVE = 0x01
} FDC_DRIVE;

typedef enum {
    FDC_DIGITAL_OUTPUT_REGISTER = 0x03F2,
    FDC_MAIN_STATUS = 0x03F4,
    FDC_DATARATE_SELECT = 0x03F4,
    FDC_DATA_FIFO = 0x03F5,
    FDC_CONFIGURATION_CONTROL_REGISTER = 0x03F7
} FDC_REGISTER;

typedef enum {
    FDC_READ_TRACK = 0x02,
    FDC_SPECIFY = 0x03,
    FDC_SENSE_DRIVE_STATUS = 0x04,
    FDC_WRITE_DATA = 0x05,
    FDC_READ_DATA = 0x06,
    FDC_RECALIBRATE = 0x07,
    FDC_SENSE_INTERRUPT = 0x08,
    FDC_WRITE_DELETED_DATA = 0x09,
    FDC_READ_ID = 0x0A,
    FDC_READ_DELETED_DATA = 0x0C,
    FDC_FORMAT_TRACK = 0x0D,
    FDC_SEEK = 0x0F,
    FDC_VERSION = 0x10,
    FDC_SCAN_EQUAL = 0x11,
    FDC_PERPENDICULAR_MODE = 0x12,
    FDC_CONFIGURE = 0x13,
    FDC_UNLOCK = 0x14,
    FDC_VERIFY = 0x16,
    FDC_SCAN_LOW_OR_EQUAL = 0x19,
    FDC_SCAN_HIGH_OR_EQUAL = 0x1D,
    FDC_LOCK = 0x94
} FDC_COMMAND;

typedef enum {
    FDC_BIT_MT = 0x80, // Multitrack Mode
    FDC_BIT_MF = 0x40, // "MFM" Magnetic Encoding Mode
    FDC_BIT_SK = 0x20  // Skip Mode
} FDC_COMMAND_OPTION;

typedef enum {
    FDC_MSR_ACTA = 0x01,
    FDC_MSR_ACTB = 0x02,
    FDC_MSR_ACTC = 0x04,
    FDC_MSR_ACTD = 0x08,
    FDC_MSR_CB = 0x10,
    FDC_MSR_NDMA = 0x20,
    FDC_MSR_DIO = 0x40,
    FDC_MSR_RQM = 0x80
} FDC_MSR;

typedef enum {
    FDC_DOR_DSEL0 = 0x01,
    FDC_DOR_DSEL1 = 0x02,
    FDC_DOR_RESET = 0x04,
    FDC_DOR_IRQ = 0x08,
    FDC_DOR_MOTA = 0x10,
    FDC_DOR_MOTB = 0x20,
    FDC_DOR_MOTC = 0x40,
    FDC_DOR_MOTD = 0x80
} FDC_DOR_BITS;

typedef enum {
    FDC_ENABLE_POLLING = 0x00,
    FDC_DISABLE_POLLING = 0x10,
    FDC_ENABLE_FIFO = 0x00,
    FDC_DISABLE_FIFO = 0x20,
    FDC_IMPLIED_SEEK = 0x40
} FDC_CONFIGURE_OPTION;

bool FDC_Initialize(void);

void FDC_SendCommandByte(FDC_COMMAND Command);

void FDC_SendParameterByte(uint8_t Parameter);

uint8_t FDC_GetResultByte(void);

bool FDC_WaitReadyForInput(void);

bool FDC_WaitReadyForOutput(void);

uint8_t FDC_GetVersion(void);

bool FDC_Configure(void);

bool FDC_Lock(void);

bool FDC_ResetController(void);

bool FDC_SelectDrive(FDC_DRIVE Drive);

bool FDC_Recalibrate(FDC_DRIVE Drive);

void FDC_SenseInterrupt(void);

void FDC_StartMotor(FDC_DRIVE Drive);

void FDC_StopMotor(FDC_DRIVE Drive);

bool FDC_CheckMedia(/*FDC_DRIVE Drive*/);

void FDC_Specify(void);

void FDC_Seek(FDC_DRIVE Drive, uint8_t Cylinder);

// void FDC_Read( void );
void FDC_ReadTrack(FDC_DRIVE Drive, uint8_t Cylinder, uint8_t Head);

void FDC_WriteTrack(FDC_DRIVE Drive, uint8_t Cylinder, uint8_t Head);

void FDC_ReadTrackDMA(FDC_DRIVE Drive, uint8_t Cylinder, uint8_t Head);

void FDC_WriteTrackDMA(FDC_DRIVE Drive, uint8_t Cylinder, uint8_t Head);

#endif // __DISK_H__