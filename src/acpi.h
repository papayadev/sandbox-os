/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#ifndef __ACPI_H__
#define __ACPI_H__

#include <stdint.h>
#include "common.h"

uint32_t GetRsdpAddress(void);

uint32_t GetRsdtAddress(void);

uint32_t GetDsdtAddress(void);

uint32_t GetFacpAddress(void);

uint32_t GetS5Address(void);

#endif // __ACPI_H__
