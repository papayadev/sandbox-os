/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#ifndef __STRING_H__
#define __STRING_H__

#include <stdint.h>

signed int ReadLine(char *, unsigned int);

void PrintString(char *);

signed int CompareStrings(char *, char *);

#endif // __STRING_H__
