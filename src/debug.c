/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#include <stdint.h>

#include "common.h"
#include "debug.h"
#include "display.h"
#include "string.h"

void PrintMemoryDump(uint32_t Base, uint32_t Size) {
  if (Base & 0x0F || Size % 16)
    Size += 16 - (Size % 16);
  Base &= 0xFFFFFFF0;
  // Print Memory Table
  char Buffer[8];
  PrintString("          00 01 02 03  04 05 06 07  08 09 0A 0B  0C 0D 0E 0F\n");
  PrintString("          -----------  -----------  -----------  -----------\n");
  for (uint32_t Index = 0; Index < Size; Index++) {
    if ((Index + 1) % 16 == 1) {
      PutUINT32(Base + Index);
      PrintString(": ");
    }
    PutBYTE(*(uint8_t *) (Base + Index));
    PutChar(' ');
    if ((Index + 1) % 4 == 0)
      PutChar(' ');
    if ((Index + 1) % 16 == 0) {
      for (int32_t Offset = -15; Offset <= 0; Offset++) {
        if (*(uint8_t *) (Base + Index + Offset) >= 0x20)
          PutChar(*(char *) (Base + Index + Offset));
        else
          PutChar('.');
      }
      PutChar('\n');
    }
    if (((Index + 1) % (16 * 16) == 0) && !((Index + 1) == Size)) {
      ReadLine(Buffer, 0);
      PrintString("          00 01 02 03  04 05 06 07  08 09 0A 0B  0C 0D 0E 0F\n");
      PrintString("          -----------  -----------  -----------  -----------\n");
    }
  }
  return;
}
