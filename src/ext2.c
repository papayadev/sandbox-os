/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

////////////////////////////////////////////////////////////////////////////////
//
//  This driver assumes that it is using the Master FDD
//

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "common.h"
#include "ext2.h"
#include "debug.h"
#include "disk.h"
#include "display.h"
#include "keyboard.h"
#include "memory.h"
#include "string.h"

// uint32_t EXT2_BlockSize = 0;
// uint32_t EXT2_BlocksPerGroup = 0;
// uint32_t EXT2_InodesPerGroup = 0;

EXT2_SUPERBLOCK Superblock;
EXT2_SUPERBLOCK_EXTENDED SuperblockExt;

bool EXT2_ParseSuperblock(void) {
  FDC_ReadTrackDMA(FDC_MASTER, 0, 0);
  // MemoryDump( 0x00200400, 0x54 );
  // uint8_t* pSuperblock = (uint8_t*) &Superblock;
  // uint8_t* pSuperblockDMA = (uint8_t*) 0x00200400;

  // for ( uint32_t Index = 0; Index < 84; Index++ )
  //{
  //    pSuperblock[Index] = pSuperblockDMA[Index];
  //}

  // EXT2_SUPERBLOCK TempSB;
  // TempSB = *(EXT2_SUPERBLOCK*) 0x00200000;
  // EXT2_SUPERBLOCK_EXTENDED TempSBEXT;
  // TempSBEXT = *(EXT2_SUPERBLOCK_EXTENDED*) 0x00200054;

  // Superblock = TempSB;
  // SuperblockExt = TempSBEXT;
  // Superblock.Signature = TempSB.Signature;

  Superblock = *(EXT2_SUPERBLOCK *) 0x00200400;
  SuperblockExt = *(EXT2_SUPERBLOCK_EXTENDED *) 0x00200454;

  if (Superblock.Signature != 0xEF53) {
    return false;
  }

  Superblock.BlockSize = 1024 << Superblock.BlockSize;
  Superblock.FragmentSize = 1024 << Superblock.FragmentSize;

  return true;
}

// uint32_t EXT2_GetBGDTBlockAddress();

bool LBAtoCHS(uint32_t LBA, DISK_CHS *ptrCHS) {
  // false if pointer is invalid (NULL)
  RETURN_FALSE(ptrCHS == NULL);

  // Cylinder = LBA / ( SectorsPerTrack * HeadsPerCylinder )
  ptrCHS->Cylinder = LBA / (ptrCHS->SPT * ptrCHS->HPC);

  // Head = ( LBA / SectorsPerTrack ) % HeadsPerCylinder
  ptrCHS->Head = (LBA / ptrCHS->SPT) % ptrCHS->HPC;

  // Sector = ( LBA % SectorsPerTrack ) + 1
  ptrCHS->Sector = (LBA % ptrCHS->SPT) + 1;

  return true;
}

bool GetInode(uint32_t InodeID, EXT2_INODE *ptrInode) {
  // Assume Floppy Disk with one Block Group

  const uint32_t SectorSize = 512;
  const uint32_t HeadPerCyl = 2;
  const uint32_t SecPerTrack = 18;

  uint32_t LBA = 0;
  DISK_CHS FloppyCHS = {0, 0, 0, HeadPerCyl, SecPerTrack};

  // Read 1st track of disk
  FDC_ReadTrackDMA(FDC_MASTER, 0, 0); // CH(S)

  // Locate {Block Group Discriptor Table}
  LBA = (Superblock.ThisBlock + 1) * Superblock.BlockSize / SectorSize;
  EXT2_BLOCK_GROUP_DISCRIPTOR BGD =
      *(EXT2_BLOCK_GROUP_DISCRIPTOR *) (0x200000 + (LBA * SectorSize));

  // Read track containing {Inode Table}
  LBAtoCHS(BGD.InodeTableStartBlock * Superblock.BlockSize / 512, &FloppyCHS);
  FDC_ReadTrackDMA(FDC_MASTER, FloppyCHS.Cylinder, FloppyCHS.Head);

  // Locate {Inode Table} on track
  uint32_t InodeTableOffset = ((FloppyCHS.Sector - 1) % 18) * SectorSize;

  // Read Inode 2
  *ptrInode =
      *(EXT2_INODE *) (0x200000 + InodeTableOffset + (InodeID - 1) * 128);

  return true;
}

uint32_t ParseDirectory(EXT2_INODE *Inode, char *FileName) {
  const uint32_t SectorSize = 512;
  const uint32_t HeadPerCyl = 2;
  const uint32_t SecPerTrack = 18;

  // uint64_t_t DirectorySize = ( Inode.SizeHigh32 << 32 ) + Inode.SizeLow32;
  // uint32_t BlockCount = DirectorySize / 1024;

  // uint32_t      LBA = 0;
  DISK_CHS FloppyCHS = {0, 0, 0, HeadPerCyl, SecPerTrack};

  // Read track containing {Inode}.{Block0}, {Directory Entry Table}
  LBAtoCHS(
      Inode->Block0 * (Superblock.BlockSize / SectorSize),
      &FloppyCHS);
  FDC_ReadTrackDMA(FDC_MASTER, FloppyCHS.Cylinder, FloppyCHS.Head);

  // Locate {Directory Entry Table} on track
  uint32_t DirectoryTableOffset =
      ((FloppyCHS.Sector - 1) % 18) * SectorSize;

  // Parse {Directory Entry Table} to find specified file/directory
  // Save {Inode Number} to InodeID if found, otherwise leave NULL
  uint32_t InodeID = 0;
  for (uint32_t Index = 0; Index < Inode->SizeLow32; Index++) {
    uint16_t EntrySize =
        *(uint16_t *) (0x200000 + DirectoryTableOffset + Index + 4);
    uint16_t NameLength =
        *(uint8_t *) (0x200000 + DirectoryTableOffset + Index + 6);
    char Name[264];

    for (uint16_t Count = 0; Count < NameLength; Count++) {
      Name[Count] = *(char *) (0x200000 + DirectoryTableOffset + Index + 8 + Count);
      if ((Count + 1) >= NameLength) {
        Name[Count + 1] = 0x00;
      }
    }

    if (CompareStrings(FileName, Name) == 0) {
      InodeID = *(uint32_t *) (0x200000 + DirectoryTableOffset + Index);
      // PrintString( "Found File:   " );
      // PrintString( Name ); PutChar( '\n' );
      // PrintString( "InodeID:      " );
      // PrintUINT32( InodeID ); PutChar( '\n' );
      break;
    }
    Index += EntrySize - 1;
  }

  return InodeID;
}

bool ParseInodeData(EXT2_INODE *Inode) {
  // Assume that the data is contained within just one block
  // and that Size is 32-bit

  const uint32_t SectorSize = 512;
  const uint32_t HeadPerCyl = 2;
  const uint32_t SecPerTrack = 18;

  DISK_CHS FloppyCHS = {0, 0, 0, HeadPerCyl, SecPerTrack};

  // Read track containing {Inode}.{Block0}
  uint32_t LBA = Inode->Block0 * (Superblock.BlockSize / SectorSize);
  LBAtoCHS(LBA, &FloppyCHS);
  FDC_ReadTrackDMA(FDC_MASTER, FloppyCHS.Cylinder, FloppyCHS.Head);

  // Locate data on track
  uint32_t DataOffset = ((FloppyCHS.Sector - 1) % 18) * SectorSize;

  // PrintString( "Block ID:     " );
  // PrintUINT32( Inode->Block0 ); PutChar( '\n' );
  // PrintString( "Cylinder:     " );
  // PrintUINT32( FloppyCHS.Cylinder ); PutChar( '\n' );
  // PrintString( "Head:         " );
  // PrintUINT32( FloppyCHS.Head ); PutChar( '\n' );
  // PrintString( "Sector:       " );
  // PrintUINT32( FloppyCHS.Sector ); PutChar( '\n' );
  // PrintString( "LBA:          " );
  // PrintUINT32( LBA ); PutChar( '\n' );

  // MemoryDump( 0x200000 + DataOffset, 0x1FF );
  // GetKey();

  uint32_t FileSize = Inode->SizeLow32;

  for (uint32_t Index = 0; Index < FileSize; Index++) {
    PutChar(*(char *) (0x200000 + DataOffset + Index));
  }

  return true;
}

bool DumpFile(char *FileName) {
  EXT2_INODE Inode = {0};
  uint32_t InodeID = 0;

  // Get {Superblock}, copied to struct "Superblock"
  EXT2_ParseSuperblock();

  // Get {Inode} of {Root Directory}
  InodeID = 2;
  GetInode(InodeID, &Inode);

  // Get {Inode} of specified file
  InodeID = ParseDirectory(&Inode, FileName);
  RETURN_FALSE(InodeID == 0);
  GetInode(InodeID, &Inode);

  ParseInodeData(&Inode);

  return true;
}
