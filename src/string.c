/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#include <stdint.h>

#include "common.h"
#include "display.h"
#include "keyboard.h"
#include "string.h"

signed int ReadLine(char *LineBuffer, unsigned int BufferLength) {
  unsigned short KeyPress;
  unsigned char KeyCharacter;
  // unsigned char	KeyScanCode;

  unsigned int BufferIndex = 0;

  do {
    KeyPress = GetKey();

    KeyCharacter = KeyPress & 0xFF;
    // KeyScanCode= KeyPress >> 8;

    if (BufferIndex > 0 && (KeyCharacter == 0x08)) {
      BufferIndex--;
      LineBuffer[BufferIndex] = 0x00;
      PutChar(KeyCharacter);
    } else if (KeyCharacter == 0x0D) {
      LineBuffer[BufferIndex] = 0x00;
      BufferIndex++;
      PutChar(KeyCharacter);
    } else if ((BufferIndex < (BufferLength - 1)) && (KeyCharacter >= 0x20)) {
      LineBuffer[BufferIndex] = KeyCharacter;
      BufferIndex++;
      PutChar(KeyCharacter);
    }

  } while (KeyCharacter != 0x0D);

  return BufferIndex;
}

void PrintString(char *String) {
  unsigned int Index;

  for (Index = 0; String[Index] != 0x00; Index++)
    PutChar(String[Index]);

  return;
}

signed int CompareStrings(char *String1, char *String2) {
  unsigned int Index = 0;

  while (1) {
    if (String1[Index] == 0 && String2[Index] == 0) {
      Index = 0;
      break;
    }

    if (String1[Index] != String2[Index]) {
      Index++;
      break;
    }

    Index++;
  }

  return Index;
}
