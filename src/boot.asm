;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Sandbox OS
;;
;;  Developed by:   Papaya
;;  Email:          me (AT) papayadev.net
;;

BITS 32

EXTERN  IDT
EXTERN  gdt_register
EXTERN  idt_register

EXTERN  kmain
EXTERN  PutChar
EXTERN  Timer_ISR
EXTERN  Keyboard_ISR
EXTERN  FDC_ISR
EXTERN  IOWait
EXTERN  PrintString
EXTERN  panic

EXTERN  PageFault_ISR

GLOBAL  start
GLOBAL  terminate
GLOBAL  reboot

EXTERN  KCODE
EXTERN  KDATA

GLOBAL  UCODE
GLOBAL  UDATA

GLOBAL  MBMagic
GLOBAL  ptrMBData

EXTERN  code
EXTERN  data
EXTERN  bss
EXTERN  end

%macro  Call_IOWait 1
  push    %1
  call    IOWait
  add     ESP, 0x04
%endmacro

%macro  Call_PrintString 1
  push    %1
  call    PrintString
  add     ESP, 0x04
%endmacro

jmp start

MBOOT_PAGE_ALIGN    EQU 1<<0
MBOOT_MEM_INFO      EQU 1<<1
MBOOT_HEADER_MAGIC  EQU 0x1BADB002
MBOOT_HEADER_FLAGS  EQU MBOOT_PAGE_ALIGN | MBOOT_MEM_INFO | 1<<16
MBOOT_CHECKSUM      EQU -(MBOOT_HEADER_MAGIC + MBOOT_HEADER_FLAGS)

ALIGN   0x04

multiboot:
  dd  MBOOT_HEADER_MAGIC
  dd  MBOOT_HEADER_FLAGS
  dd  MBOOT_CHECKSUM
  dd  multiboot
  dd  code
  dd  bss
  dd  end
  dd  start

MBMagic:
  dd  0x00000000
ptrMBData:
  dd  0x00000000

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  procedure: start()
;;
;;  Initialize the kernel runtime enviroment.
;;
;;  Input: N/A
;;  Output: N/A
;;  Registers changed: N/A

start:
  cli

  mov [MBMagic], EAX
  mov [ptrMBData], EBX

  mov EAX, PageFault_ISR  ;; Page Fault
  mov [IDT+0x0070], AX    ;; IDT gate INT 0x0E (Decimal 14)
  shr EAX, 0x10
  mov [IDT+0x0076], AX

  mov EAX, Timer_ISR      ;; Programmable Interval Timer
  mov [IDT+0x0100], AX    ;; Setting IDT gate (IRQ0) (INT 20)
  shr EAX, 0x10
  mov [IDT+0x0106], AX

  mov EAX, Keyboard_ISR   ;; Keyboard
  mov [IDT+0x0108], AX    ;; Setting IDT gate (IRQ1) (INT 21)
  shr EAX, 0x10
  mov [IDT+0x010E], AX

  mov EAX, FDC_ISR        ;; FDD Controller
  mov [IDT+0x0130], AX    ;; Setting IDT gate (IRQ6) (INT 26)
  shr EAX, 0x10
  mov [IDT+0x0136], AX

  mov AL, 0x11        ;; Programmable Interrupt Controller
  out 0x20, AL        ;; Initialize
  out 0xA0, AL

  mov AL, 0x20        ;; PIC IDT vector offsets
  out 0x21, AL
  mov AL, 0x28
  out 0xA1, AL

  mov AL, 0x04        ;; PIC master/slave connection
  out 0x21, AL
  mov AL, 0x02
  out 0xA1, AL

  mov AL, 0x01        ;; PIC mode
  out 0x21, AL
  out 0xA1, AL

  mov AL, 0x00        ;; PIC mask
  out 0x21, AL
  out 0xA1, AL

  mov AL, 0x36        ;; Programmable Interval Timer
  out 0x43, AL        ;; Initialize

  mov AX, 0xE90B      ;; Set PIT frequency to 20 Hz
  out 0x40, AL
  mov AL, AH
  out 0x40, AL

  lgdt [gdt_register]      ;; Globral/Interrupt discriptor tables
  lidt [idt_register]

  mov AX, KDATA       ;; Initialize segment registers
  mov DS, AX          ;; and stack
  mov ES, AX
  mov FS, AX
  mov GS, AX
  mov SS, AX
  mov ESP, 0x00080000

  jmp KCODE:.continue     ;; Jump into new kernel segment
                          ;; and set interrupts
.continue:
  sti

  ;;xor EAX, EAX            ;; Zero out base memory
  ;;xor EDI, EDI
  ;;mov ECX, 0x000A0000
  ;;rep
  ;;stosb

  ;; Page directory

  ;; Page table

  ;;mov EAX, 0x00000003     ;; ???

  ;;push    EBX

  call    kmain            ;; Call kernel

  jmp terminate


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  procedure: reboot()
;;
;;  Attempts to reboot, or terminates on failure.
;;

reboot:
  cli
  mov     ECX, 0xFFFF

.wait:
  in      AL, 0x64
  test    AL, 0x02
  loopnz  .wait

  mov     AL, 0xFE
  out     0x64, AL

  call    terminate

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  procedure: terminate()
;;
;;  Stops the system.
;;
;;  Input: N/A
;;  Output: N/A
;;  Registers changed: N/A

MSG_TERM:
  db      0x0D, "TERMINATED!", 0x00

terminate:
  cli
  Call_PrintString(MSG_TERM)

.loop:
  hlt
  jmp     .loop
