;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Sandbox OS
;;
;;  Developed by:   Papaya
;;  Email:          me (AT) papayadev.net
;;

BITS 32

GLOBAL inb			;; uint8_t inb(uint16_t port);
GLOBAL outb			;; outb(uint8_t data, uint16_t port);

inb:
inb_p:
	push	EBP
	mov	EBP, ESP
	xor	EAX, EAX
	mov	DX, [EBP+0x08]
	in	AL, DX
	pop	EBP
	ret

outb:
outb_p:
	push	EBP
	mov	EBP, ESP
	mov	AL, [EBP+0x08]
	mov	DX, [EBP+0x0C]
	out	DX, AL
	pop	EBP
	ret
