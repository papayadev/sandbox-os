/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#include <stddef.h>
#include <stdint.h>

#include "acpi.h"
#include "common.h"
#include "debug.h"
#include "disk.h"
#include "display.h"
#include "isa_dma.h"
#include "ext2.h"
#include "string.h"
#include "testing.h"

void ACPI_TEST1(void) {
  uint32_t Base = 0;

  PrintString("\nRSDP is located at: ");
  PutUINT32(Base = GetRsdpAddress());
  PrintString("\n");
  PrintMemoryDump(Base, 0x30);

  PrintString("\nRSDT is located at: ");
  PutUINT32(Base = GetRsdtAddress());
  PrintString("\n");
  PrintMemoryDump(Base, *(uint32_t *) (Base + 0x04));

  PrintString("\nFACP is located at: ");
  PutUINT32(Base = GetFacpAddress());
  PrintString("\n");
  PrintMemoryDump(Base, *(uint32_t *) (Base + 0x04));

  //            PrintString("\nDSDT is located at: ");
  //            PutUINT32( Base = DSDT_Location() );
  //            PrintString( "\n" );
  //            MemoryDump( Base, *(uint32_t*)(Base + 0x04) );

  PrintString("\n\\_S5_ is located at: ");
  PutUINT32(Base = GetS5Address());
  PrintString("\n");
  PrintMemoryDump(Base, 0x40);

  PrintString("\n");

  return;
}

void EXT2_TEST1(void) // 1st Superblock
{
  // FDC_ReadTrackDMA( FDC_MASTER, 0, 0 );

  PrintString("Block Address of SB:    ");
  PrintUINT32(Superblock.ThisBlock);

  PrintString("\nTotal Inodes:           ");
  PrintUINT32(Superblock.TotalInodes);

  PrintString("\nTotal Blocks:           ");
  PrintUINT32(Superblock.TotalBlocks);

  PrintString("\nUnallocated Inodes:     ");
  PrintUINT32(Superblock.UnallocatedInodes);

  PrintString("\nUnallocated Blocks:     ");
  PrintUINT32(Superblock.UnallocatedBlocks);

  PrintString("\nBlock Size:             ");
  PrintUINT32(Superblock.BlockSize);

  PrintString("\nBlocks per Group:       ");
  PrintUINT32(Superblock.BlocksPerGroup);

  PrintString("\nInodes per Group:       ");
  PrintUINT32(Superblock.InodesPerGroup);

  PrintString("\nFirst Available Inode:  ");
  PrintUINT32(SuperblockExt.FirstNonReservedInode);

  PrintString("\nInode Size:             ");
  PrintUINT32(SuperblockExt.InodeSize);

  PrintString("\n");
  return;
}

void EXT2_TEST2(void) // 1st Block Group Discriptor (0)
{
  uint32_t BGDBlock = Superblock.ThisBlock + 1;
  uint32_t BGDOffset = (BGDBlock) * Superblock.BlockSize;
  EXT2_BLOCK_GROUP_DISCRIPTOR BGD =
      *(EXT2_BLOCK_GROUP_DISCRIPTOR *) (0x00200000 + BGDOffset);

  PrintString("BGDT Block Address:     ");
  PrintUINT32(Superblock.ThisBlock + 1);

  PrintString("\nBlock Bitmap Block:     ");
  PrintUINT32(BGD.BlockBitmapBlock);

  PrintString("\nInode Bitmap Block:     ");
  PrintUINT32(BGD.InodeBitmapBlock);

  PrintString("\nInode Table 1st Block:  ");
  PrintUINT32(BGD.InodeTableStartBlock);

  PrintString("\nUnallocated Blocks:     ");
  PrintUINT32(BGD.UnallocatedBlocks);

  PrintString("\nUnallocated Inodes:     ");
  PrintUINT32(BGD.UnallocatedInodes);

  PrintString("\nDirectories in Group    ");
  PrintUINT32(BGD.DirectoryCount);

  PutChar('\n');
  return;
}

void FDC_TEST1(void) {
  ///////////////////////////////////////////
  //
  //  Read/Write a floppy disk:
  //  DMA_FDC_Configure_Read
  //  DMA_FDC_Initialize
  //  FDC_SelectDrive
  //  FDC_Specify
  //  FDC_StartMotor
  //  FDC_Seek
  //  FDC_ReadTrack/FDC_WriteTrack
  //  FDC_StopMotor

  PrintMemoryDump(0x200000, 0x10);
  PrintString("Configure DMA...      ");
  DMA_FDC_Configure_Read();
  PrintString("[OK]\n");
  PrintString("Initialize DMA...     ");
  DMA_FDC_Initialize();
  PrintString("[OK]\n");

  PrintString("Select Master...      ");
  FDC_SelectDrive(FDC_MASTER);
  PrintString("[OK]\n");
  PrintString("Specify...            ");
  FDC_Specify();
  PrintString("[OK]\n");
  PrintString("Start Motor...        ");
  FDC_StartMotor(FDC_MASTER);
  PrintString("[OK]\n");
  PrintString("Read...               ");
  FDC_ReadTrack(FDC_MASTER, 0, 0);
  // Read Master Cyl 0 Head 0 (Track 1)
  PrintString("[OK]\n");
  PrintString("Stop Motor...         ");
  FDC_StopMotor(FDC_MASTER);
  PrintString("[OK]\n");
  PrintMemoryDump(0x200000, 0x10);
  uint32_t *pMemory = (uint32_t *) 0x200000;
  pMemory[0] = 0x0D0A0E0D;

  DMA_FDC_Configure_Write();
  DMA_FDC_Initialize();
  FDC_StartMotor(FDC_MASTER);
  FDC_WriteTrack(FDC_MASTER, 0, 0);
  FDC_StopMotor(FDC_MASTER);

  return;
}
