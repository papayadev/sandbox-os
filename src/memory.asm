;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Sandbox OS
;;
;;  Developed by:   Papaya
;;  Email:          me (AT) papayadev.net
;;

BITS	32

GLOBAL	Init_Paging
GLOBAL  PageFault_ISR

EXTERN	KCODE
EXTERN	KDATA

EXTERN  PrintString
EXTERN  panic
EXTERN  PutUINT32

ALIGN   0x1000
PD_Kernel:
  times 0x0400 dd	0x00000000

ALIGN   0x01000
PT_1:
  times 0x400 dd 0x00000000


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;	ASM procedure: Init_Paging()
;;
;;	Initializes paging using a 1:1 table for the first 4MB of memory.
;;
;;	Registers changed: None
;;

Init_Paging:
  push  eax
  push  ebx
  push  ecx
  push  edi

  mov   ebx, PT_1       ;; store address of page table
  mov   eax, ebx
  or    eax, 0x03       ;; set flags of directory entry
  mov   [PD_Kernel], eax  ;; write directory entry

  mov   eax, 0x03       ;; set flags for table entry
  xor   edi, edi
  xor   ecx, ecx

.loop:
  mov   [ebx+edi], eax    ;; write a table entry
  add   edi, 0x00000004   ;; increment by one dword
  inc   ecx         ;; increment loop counter
  cmp   edi, 0x00001000   ;; is pointing at 4096
  je    .done         ;; done
  add   eax, 0x00001000   ;; add 4096 to entry
  jmp   .loop         ;; repeat

.done:
  mov   eax, PD_Kernel
  mov   cr3, eax

  mov   eax, cr0
  or    eax, 0x80000000
  mov   cr0, eax

  pop   edi
  pop   ecx
  pop   ebx
  pop   eax

  ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Page Fault ISR
;;

PageFault_Msg1:
  db    "A page fault has occured at 0x", 0x00
PageFault_Msg2:
  db    ".", 0x0D, 0x00

  PageFault_ISR:
  push  PageFault_Msg1
  call  PrintString
  add   ESP, 0x04
  mov   EAX, CR2
  push  EAX
  call  PutUINT32
  add   ESP, 0x04
  push  PageFault_Msg2
  call  PrintString
  add   ESP, 0x04
  call  panic
  add   ESP, 0x04
  iret
