;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Sandbox OS
;;
;;  Developed by:   Papaya
;;  Email:          me (AT) papayadev.net
;;

BITS 	32

GLOBAL	FDC_ISR

EXTERN	KCODE
EXTERN	KDATA

EXTERN	FDC_Wait_IRQ
EXTERN  PrintString

%macro	Call_PrintString 1
  push	%1
  call	PrintString
  add		ESP, 0x04
%endmacro

MSG_FDC_ISR:
  DB      "(IRQ6)", 0x00

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;	ASM procedure: FDC_ISR()
;;
;;	Interrupt handler for the FDD (IRQ6).
;;
;;	Input: N/A
;;	Output: N/A
;;	Registers changed: None

FDC_ISR:
	pushad

  ;;  Call_PrintString( MSG_FDC_ISR )

	mov	byte [FDC_Wait_IRQ], 0x00		;; Stuff to do

	mov	AL, 0x20		                ;; EOI
	out	0x20, AL

	popad				                ;; pop all registers
	iret				                ;; and return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%define	REG_STATUS_A		0x03F0
%define	REG_STATUS_B		0x03F1
%define	REG_DIGITAL_OUTPUT	0x03F2
%define	REG_TAPE_DRIVE		0x03F3
%define	REG_MAIN_STATUS		0x03F4
%define	REG_DATARATE_SELECT	0x03F4
%define	REG_DATA_FIFO		0x03F5
%define	REG_DIGITAL_INPUT	0x03F7
%define	REG_CONFIG_CONTROL	0x03F7

%define	DOR_DSEL0		0x01
%define	DOR_DSEL1		0x02
%define	DOR_RESET		0x04
%define	DOR_IRQ			0x08
%define	DOR_MOTA		0x10
%define	DOR_MOTB		0x20
%define	DOR_MOTC		0x40
%define	DOR_MOTD		0x80

%define	MSR_ACTA		0x01
%define	MSR_ACTB		0x02
%define	MSR_ACTC		0x04
%define	MSR_ACTD		0x08
%define	MSR_CB			0x10
%define	MSR_NDMA		0x20
%define	MSR_DIO			0x40
%define	MSR_RQM			0x80

%define COM_READ_TRACK		0x02
%define COM_SPECIFY		0x03
%define COM_SENSE_DRIVE_STATUS	0x04
%define COM_WRITE_DATA		0x05
%define COM_READ_DATA		0x06
%define COM_RECALIBRATE		0x07
%define COM_SENSE_INTERRUPT	0x08
%define COM_WRITE_DELETED_DATA	0x09
%define COM_READ_ID		0x0A
%define COM_READ_DELETED_DATA	0x0C
%define COM_FORMAT_TRACK	0x0D
%define COM_SEEK		0x0F
%define COM_VERSION		0x10
%define COM_SCAN_EQUAL		0x11
%define COM_PERPENDICULAR_MODE	0x12
%define COM_CONFIGURE		0x13
%define COM_LOCK		0x14
%define COM_VERIFY		0x16
%define COM_SCAN_LOW_OR_EQUAL	0x19
%define COM_SCAN_HIGH_OR_EQUAL	0x1D
