/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#ifndef __ISA_DMA_H__
#define __ISA_DMA_H__

#include <stdint.h>

void DMA_FDC_Initialize(void);

void DMA_FDC_Configure_Write(void);

void DMA_FDC_Configure_Read(void);

#endif // __ISA_DMA_H__
