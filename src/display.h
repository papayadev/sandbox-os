/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include <stdint.h>

#include "common.h"

#define DEFAULT_CHAR_ATTRIB 0x0F

#define FG_BLACK 0x00
#define FG_BLUE 0x01
#define FG_GREEN 0x02
#define FG_CYAN 0x03
#define FG_RED 0x04
#define FG_MAGENTA 0x05
#define FG_BROWN 0x06
#define FG_LIGHTGRAY 0x07
#define FG_DARKGRAY 0x08
#define FG_LIGHTBLUE 0x09
#define FG_LIGHTGREEN 0x0A
#define FG_LIGHTCYAN 0x0B
#define FG_LIGHTRED 0x0C
#define FG_LIGHTMAGENTA 0x0D
#define FG_YELLOW 0x0E
#define FG_WHITE 0x0F

#define BG_BLACK 0x00
#define BG_BLUE 0x10
#define BG_GREEN 0x20
#define BG_CYAN 0x30
#define BG_RED 0x40
#define BG_MAGENTA 0x50
#define BG_BROWN 0x60
#define BG_LIGHTGRAY 0x70

#define FG_BLINK 0x80

typedef enum {
    Invalid = 0,
    Up = 1,
    Down = 2,
    Left = 3,
    Right = 4
} DIRECTION;

extern uint8_t cursor_x;
extern uint8_t cursor_y;

void SetAttrib(uint8_t Attrib);

void SetCursor(void);

void ClearDisplay(void);

void ScrollDisplay(void);

void PutChar(uint8_t);

void PutBYTE(uint8_t);

void PutUINT16(uint16_t);

void PutUINT32(uint32_t);

void PrintUINT32(uint32_t Value);

void SetCursorXY(uint16_t X, uint16_t Y);

void ClearConsole(void);

void ScrollConsole(DIRECTION Direction, uint16_t Count);

void InitializeStatusBar(void);

void PrintStatusBar(char *);

#endif // __DISPLAY_H__
