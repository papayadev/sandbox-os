/*//////////////////////////////////////////////////////////////////////////////
//
//  Sandbox OS
//
//  Developed by:   Papaya
//  Email:          me (AT) papayadev.net
*/

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "acpi.h"
#include "common.h"
#include "debug.h"
#include "disk.h"
#include "display.h"
#include "ext2.h"
#include "isa_dma.h"
#include "keyboard.h"
#include "memory.h"
#include "multiboot.h"
#include "string.h"
#include "testing.h"

void panic(void);

void terminate(void);

void IOWait(uint32_t);

void reboot(void);

extern uint32_t MBMagic;
extern multiboot_info_t *ptrMBData;

uint32_t GetWord(char *Buffer, uint32_t BufSize, char *Text, uint32_t Skip);

uint32_t GetWordCount(char *Text);

uint32_t MemorySize = 0;

int kmain(void);

extern uint32_t _code;
extern uint32_t _data;
extern uint32_t _bss;
extern uint32_t _end;

int kmain(void) {
  if (ptrMBData->flags & 0x00000001) {
    MemorySize = ptrMBData->mem_upper + 1024;
  }

  uint8_t data;

  char buffer1[256];

  // ????????????
  // data= in(0x03DA);
  // data= in(0x03C0);
  // data= (data & 0xE0) | 0x10;
  // outb(data, 0x03C0);
  // data= in(0x03C1);
  // data= data & 0xF7;
  // outb(data, 0x03C0);

  ClearDisplay();

  /* VGA Input Status 1 */
  data = inb(0x03DA);

  /* VGA Index/Data */
  data = inb(0x03C0);
  data = (data & 0xE0) | 0x10; /* BITS 0-4 OFF, BIT 4 ON */
  outb(data, 0x03C0);

  /* VGA Other Attribute */
  data = inb(0x03C1);
  data |= 0x08; /* BIT 3 OFF */
  outb(data, 0x03C0);

  InitializeStatusBar();

  PutChar('\n');
  SetAttrib(BG_BLUE | FG_WHITE);
  PutChar(0xDA);
  for (uint8_t Temp = 1; Temp < 79; Temp++) {
    PutChar(0xC4);
  }
  PutChar(0xBF);

  PutChar(0xB3);
  PrintString(" SANDBOX OS");
  for (uint8_t Temp = cursor_x; Temp < 79; Temp++) {
    PutChar(0x20);
  }
  PutChar(0xB3);
  PutChar(0xB3);
  PrintString(" Author:           Papaya");
  for (uint8_t Temp = cursor_x; Temp < 79; Temp++) {
    PutChar(0x20);
  }
  PutChar(0xB3);
  PutChar(0xB3);
  PrintString(" Version:          00.22.03.24.01");
  for (uint8_t Temp = cursor_x; Temp < 79; Temp++) {
    PutChar(0x20);
  }
  PutChar(0xB3);
  PutChar(0xB3);
  PrintString(" Total Memory:     ");
  PrintUINT32(MemorySize);
  PrintString(" KB");
  for (uint8_t Temp = cursor_x; Temp < 79; Temp++) {
    PutChar(0x20);
  }
  PutChar(0xB3);
  PutChar(0xB3);
  PrintString(" Kernel Loaded:    0x");
  PutUINT32((uint32_t) &_code);
  PrintString(" - 0x");
  PutUINT32((uint32_t) &_end);
  for (uint8_t Temp = cursor_x; Temp < 79; Temp++) {
    PutChar(0x20);
  }
  PutChar(0xB3);
  PutChar(0xB3);
  PrintString(" Kernel Memory:    ");
  PrintUINT32(((uint32_t) &_end - (uint32_t) &_code) / 1024);
  PrintString(" KB");
  for (uint8_t Temp = cursor_x; Temp < 79; Temp++) {
    PutChar(0x20);
  }
  PutChar(0xB3);

  PutChar(0xC0);
  for (uint8_t Temp = 1; Temp < 79; Temp++) {
    PutChar(0xC4);
  }
  PutChar(0xD9);

  SetAttrib(DEFAULT_CHAR_ATTRIB);
  PutChar('\n');

  while (1) {
    PrintString("k> ");
    ReadLine(buffer1, 77);

    char word[256];

    GetWord(word, 256, buffer1, 0);

    if (CompareStrings(word, "clear") == 0 && GetWordCount(buffer1) == 1)
      // ClearDisplay();
      ClearConsole();
    else if (CompareStrings(word, "panic") == 0 && GetWordCount(buffer1) == 1)
      panic();
    else if (CompareStrings(word, "terminate") == 0 && GetWordCount(buffer1) == 1)
      terminate();
    else if (CompareStrings(word, "reboot") == 0 && GetWordCount(buffer1) == 1)
      reboot();
    else if (CompareStrings(word, "help") == 0 && GetWordCount(buffer1) == 1) {
      PrintString("clear                          - clear the screen\n");
      PrintString("init [disk|paging]             - initialize feature\n");
      PrintString("test [acpi|bgd|disk|ext2\n");
      PrintString("      iowait|superblock ]      - test feature\n");
      PrintString("panic                          - panic (will reboot)\n");
      PrintString("terminate                      - locks the system\n");
      PrintString("reboot                         - reboot the system\n");
      PrintString("help                           - display this message\n");
    } else if (CompareStrings(word, "init") == 0 && GetWordCount(buffer1) == 2) {
      GetWord(word, 256, buffer1, 1);

      if (CompareStrings(word, "disk") == 0)
        FDC_Initialize();

      else if (CompareStrings(word, "paging") == 0) {
        PrintString("Initialize Paging...  ");
        Init_Paging();
        PrintString("[OK]\n");
      } else {
        PrintString("init [disk|paging]\n");
      }
    } else if (CompareStrings(word, "test") == 0 && GetWordCount(buffer1) == 2) {
      GetWord(word, 256, buffer1, 1);

      // PrintString ( word ); PutChar( '\n' );

      if (CompareStrings(word, "iowait") == 0) {
        IOWait(3000);
      } else if (CompareStrings(word, "acpi") == 0) {
        ACPI_TEST1();
      } else if (CompareStrings(word, "disk") == 0) {
        FDC_TEST1();
      } else if (CompareStrings(word, "superblock") == 0) {
        EXT2_ParseSuperblock();
        EXT2_TEST1();
      } else if (CompareStrings(word, "bgd") == 0) {
        EXT2_ParseSuperblock();
        EXT2_TEST2();
      } else if (CompareStrings(word, "ext2") == 0) {
        DumpFile("TestMessage.txt");
      } else if (CompareStrings(word, "debug") == 0) {
        PrintStatusBar("This is a debug test message.");
      } else {
        PrintString("Nothing to test.\n");
      }
    } else if (CompareStrings(word, "dump") == 0 && GetWordCount(buffer1) == 2) {
      GetWord(word, 256, buffer1, 1);
      bool Result = DumpFile(word);
      if (!Result) {
        PrintString("File not found.\n");
      }
    } else if (CompareStrings(word, "debug") == 0 && GetWordCount(buffer1) == 2) {
      GetWord(word, 256, buffer1, 1);
      PrintStatusBar(word);
    } else if (CompareStrings(word, "") == 0 || GetWordCount(buffer1) == 0) {
    } else {
      PrintString("I don't understand.\n");
    }
  }

  return 0x0777D00D;
}

uint32_t GetWord(char *Buffer, uint32_t BufSize, char *Text, uint32_t Skip) {
  uint32_t Index = 0;
  uint32_t WordCount = 0;

  bool FoundWord = false;

  while (Text[Index] == 0x20) {
    Index++;
  }

  for (; ((WordCount < Skip) && (Text[Index] != 0x00)); Index++) {
    if (Text[Index] == 0x20) {
      if (FoundWord) {
        WordCount++;
        FoundWord = false;
      }
      while (Text[Index + 1] == 0x20) {
        Index++;
      }
    } else {
      FoundWord = true;
    }
  }

  uint32_t WordBase = Index;
  for (Index = 0; Index < BufSize; Index++) {
    Buffer[Index] = Text[WordBase + Index];
    if (Buffer[Index] == 0x00 || Buffer[Index] == 0x20) {
      Buffer[Index] = 0x00;
      break;
    }
  }

  return 0;
}

uint32_t GetWordCount(char *Text) {
  uint32_t Index = 0;
  uint32_t WordCount = 0;

  bool FoundWord = false;

  for (Index = 0; Text[Index] != 0x00; Index++) {
    if (Text[Index] == 0x20) {
      if (FoundWord) {
        FoundWord = false;
      }
      while (Text[Index + 1] == 0x20) {
        Index++;
      }
    } else {
      if (!FoundWord) {
        WordCount++;
      }
      FoundWord = true;
    }
  }

  return WordCount;
}

void panic(void) {
  PrintString("\nPANIC!");
  IOWait(30000);
  reboot();
}
