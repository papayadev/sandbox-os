;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Sandbox OS
;;
;;  Developed by:   Papaya
;;  Email:          me (AT) papayadev.net
;;

BITS 32

GLOBAL  KCODE
GLOBAL  KDATA
GLOBAL  UCODE
GLOBAL  UDATA

GLOBAL  IDT
GLOBAL  gdt_register
GLOBAL  idt_register

;;EXTERN  Keyboard_ISR


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;	procedure: ISR_IRQX()
;;
;;	Process unhandled hardware interrupt requests.
;;
;;	Input: N/A
;;	Output: N/A
;;	Registers changed: None

ISR_IRQX:
	push	EAX

	mov	EAX, 0x20
	out	0x20, AL

	pop	EAX
	iret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;	procedure: ISR_INTX()
;;
;;	Process unhandled traps and exceptions.
;;
;;	Input: N/A
;;	Output: N/A
;;	Registers changed: None

MSG1:
	db	"A fault has occured.", 0x0D, 0x00

ISR_INTX:
	iret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

gdt_register:
	dw	GDTSIZE - 1
	dd	GDT

idt_register:
	dw	IDTSIZE - 1
	dd	IDT

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  GDT
;;

GDT:
NULLSEL		EQU $-GDT
	dq	0x0000000000000000	;; null descriptor
KCODE		EQU $-GDT
	db	11111111b		;; Limit (bits 07-00)
	db	11111111b		;; Limit (bits 15-08)
	db	00000000b		;; Base (bits 07-00)
	db	00000000b		;; Base (bits 15-08)
	db	00000000b		;; Base (bits 23-16)
	db	10011010b		;; P, DPL (2 bits), Type (5 bits)
	db	11001111b		;; G, D, 0, A, Limit (bits 19-16)
	db	00000000b		;; Base (bits 31-24)
KDATA		EQU $-GDT
	db	11111111b		;; Limit (bits 07-00)
	db	11111111b		;; Limit (bits 15-08)
	db	00000000b		;; Base (bits 07-00)
	db	00000000b		;; Base (bits 15-08)
	db	00000000b		;; Base (bits 23-16)
	db	10010010b		;; P, DPL (2 bits), Type (5 bits)
	db	11001111b		;; G, D, 0, A, Limit (bits 19-16)
	db	00000000b		;; Base (bits 31-24)
UCODE		EQU $-GDT
	dq	0x0000000000000000	;; blank descriptor
UDATA		EQU $-GDT
	dq	0x0000000000000000	;; blank descriptor
GDTSIZE		EQU $-GDT

;;TESTEQU		EQU	Keyboard_ISR

SECTION_BASE	EQU	0x100000
ISR_IRQX_LO	EQU	(SECTION_BASE + ISR_IRQX - $$) & 0xFFFF
ISR_IRQX_HI	EQU	(SECTION_BASE + ISR_IRQX - $$) >> 0x10
ISR_INTX_LO	EQU	(SECTION_BASE + ISR_INTX - $$) & 0xFFFF
ISR_INTX_HI	EQU	(SECTION_BASE + ISR_INTX - $$) >> 0x10

IDT:
	dw	ISR_INTX_LO		;; Interrupt 0x00
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x01
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x02
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x03
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x04
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x05
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x06
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x07
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x08
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x09
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x0A
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x0B
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x0C
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x0D
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x0E
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x0F
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x10
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x11
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x12
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x13
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x14
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x15
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x16
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x17
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x18
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x19
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x1A
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x1B
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x1C
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x1D
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x1E
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_INTX_LO		;; Interrupt 0x1F
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x20 (IRQ0)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_IRQX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x21 (IRQ1)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_IRQX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x22 (IRQ2)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_IRQX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x23 (IRQ3)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_IRQX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x24 (IRQ4)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_IRQX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x25 (IRQ5)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_IRQX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x26 (IRQ6)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_IRQX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x27 (IRQ7)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_IRQX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x28 (IRQ8)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_IRQX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x29 (IRQ9)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_IRQX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x2A (IRQ10)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_IRQX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x2B (IRQ11)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_IRQX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x2C (IRQ12)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_IRQX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x2D (IRQ13)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_IRQX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x2E (IRQ14)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_IRQX_HI

	dw	ISR_IRQX_LO		;; Interrupt 0x2F (IRQ15)
	dw	KCODE
	db	0x00
	db	0x8E
	dw	ISR_INTX_HI

IDTSIZE		EQU $-IDT

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
