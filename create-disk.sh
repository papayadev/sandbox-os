#!/bin/bash
set -e
IMG="cmake-build/bin/disk.img"
LAYOUT="disk.layout"
GRUBCFG="grub.cfg"
if [ ! -z "$1" ]; then IMG="$1"; fi
if [ ! -z "$2" ]; then LAYOUT="$2"; fi
if [ ! -z "$3" ]; then GRUBCFG="$3"; fi
sudo -v
rm -f "$IMG"
dd if=/dev/zero of="$IMG" bs=512 count=40960
LOOP=`sudo losetup -f`
sudo losetup -P "$LOOP" "$IMG"
sudo sfdisk "$LOOP" < "$LAYOUT"
sudo mke2fs "${LOOP}p1"
sudo mount "${LOOP}p1" /mnt
sudo mkdir -p /mnt/boot/grub
sudo cp "$GRUBCFG" /mnt/boot/grub/
sudo grub-install --target=i386-pc --root-directory=/mnt --no-floppy --modules="normal part_msdos ext2 multiboot" "$LOOP"
sudo sync
sudo umount /mnt
sudo losetup -d "$LOOP"
